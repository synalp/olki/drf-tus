# -*- coding: utf-8 -*-
from django.conf.urls import url, include
from rest_framework_tus.views import UploadViewSet

from .routers import TusAPIRouter

router = TusAPIRouter()
router.register(r'files', UploadViewSet, basename='upload')

app_name = 'tus'

urlpatterns = [
    url(r'', include((router.urls, 'api'), namespace='api'))
]
